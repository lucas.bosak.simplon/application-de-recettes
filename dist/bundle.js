/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/components/MealCard.js":
/*!************************************!*\
  !*** ./src/components/MealCard.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"MealCard\": () => (/* binding */ MealCard)\n/* harmony export */ });\nclass MealCard {\r\n  //  Le constructor\r\n  constructor(meal) {\r\n    this._idMeal = meal.idMeal;\r\n    this._strMeal = meal.strMeal;\r\n    this._strDrinkAlternate = meal.strDrinkAlternate;\r\n    this._strCategory = meal.strCategory;\r\n    this._strArea = meal.strArea;\r\n    this._strMealThumb = meal.strMealThumb;\r\n    this._strYoutube = meal.strYoutube;\r\n\r\n    this._strInstructions = meal.strInstructions;\r\n    this._strIngredients = [];\r\n    this._strMeasures = [];\r\n\r\n    for (let i = 1; i <= 20; i++) {\r\n      const ingredient = meal[\"strIngredient\" + i];\r\n      const measure = meal[\"strMeasure\" + i];\r\n\r\n      if (ingredient !== \"\" && ingredient !== null) {\r\n        this._strIngredients.push(ingredient);\r\n        this._strMeasures.push(measure);\r\n      }\r\n    }\r\n  }\r\n\r\n  // Les méthodes\r\n\r\n  // Function pour inserer en HTML la card\r\n  get content() {\r\n    const div = document.createElement(\"li\");\r\n\r\n    const markup = `\r\n    <div class=\"item\">\r\n    <a id=\"recette\" data-bs-toggle=\"modal\" data-bs-target=\"#Id${this._idMeal}\" class=\"card\">\r\n      <div class=\"thumb\" style=\"background-image: url(${this._strMealThumb});\"></div>\r\n      <article>\r\n      <h2>${this._strMeal}</h2>\r\n      <p>Catégorie: ${this._strCategory}<br>\r\n      Pays: ${this._strArea} </p>\r\n      </article>\r\n    </a>\r\n    </div>\r\n`;\r\n\r\n    div.innerHTML = markup;\r\n    return div;\r\n  }\r\n  get recipe() {\r\n    const div = document.createElement(\"li\");\r\n    const ingredientList = this._strIngredients\r\n      .map((ingredient, i) => {\r\n        const measure = this._strMeasures[i];\r\n        return `<li>${measure} ${ingredient}</li>`;\r\n      })\r\n      .join(\"\");\r\n\r\n    const markup = `\r\n  <div class=\"modal fade\" id=\"Id${this._idMeal}\" data-bs-backdrop=\"static\" data-bs-keyboard=\"false\" tabindex=\"-1\"\r\n        aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable\">\r\n          <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n              <h1 class=\"modal-title fs-5\" id=\"staticBackdropLabel\">${this._strMeal}</h1>\r\n              <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Fermer\"></button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n              <h2>Les ingredients</h2>\r\n              <ul>\r\n                  ${ingredientList}\r\n              </ul>\r\n              <hr>\r\n              <h2>La recette</h2>\r\n              <h5 id=\"recette\">${this._strInstructions}</h5>\r\n              <hr>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n              <button type=\"button\" class=\"btn btn-secondary\" data-bs-dismiss=\"modal\">Fermer</button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n`;\r\n    div.innerHTML = markup;\r\n    return div;\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack://application-de-recettes/./src/components/MealCard.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components_MealCard_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/MealCard.js */ \"./src/components/MealCard.js\");\n\r\n// import { MealDetail } from \"./components/MealDetail.js\";\r\n// clef d'API\r\nconst apiKey = \"1\";\r\n\r\n// Le début de l'URL pour utiliser MealDB\r\nconst urlStart = \"https://www.themealdb.com/api/json/v1/\";\r\n\r\nconst form = document.querySelector(\"#Form\");\r\nconst input = document.querySelector(\"#ByName\");\r\nconst list = document.querySelector(\"#band\");\r\n\r\nconst fenetre = document.querySelector(\"#fenetre\");\r\nconst cat = document.querySelector(\"#form2\");\r\nconst land = document.querySelector(\"#form3\");\r\n\r\nif (form !== null) form.addEventListener(\"submit\", byName);\r\nif (cat !== null) cat.addEventListener(\"submit\", byCategory);\r\nif (land !== null) land.addEventListener(\"submit\", byArea);\r\n\r\n// les url pour appeler l'API selon la manière de rechercher\r\nconst searchByName = \"/search.php?s=\";\r\n\r\nconst searchByCategory = \"/filter.php?c=\";\r\n\r\nconst searchByArea = \"/filter.php?a=\";\r\n\r\nconst searchRandom = \"/random.php\";\r\n\r\n// Function pour afficher une recette au hasard.\r\nfunction random() {\r\n  const urlRandom = `${urlStart}${apiKey}${searchRandom}`;\r\n\r\n  fetch(urlRandom)\r\n    .then((response) => response.json())\r\n    .then(function (data) {\r\n      // console.log(data);\r\n\r\n      const { meals } = data;\r\n\r\n      for (let meal of meals) {\r\n        let mealCard = new _components_MealCard_js__WEBPACK_IMPORTED_MODULE_0__.MealCard(meal);\r\n        list.appendChild(mealCard.content);\r\n        fenetre.appendChild(mealCard.recipe);\r\n        // console.log(mealCard)\r\n      }\r\n    })\r\n    .catch(function (error) {\r\n      console.error(\r\n        \"Il y a eu un problème avec l'opération fetch Random : \" + error.message\r\n      );\r\n    });\r\n}\r\nfor (let i = 0; i < 6; i++) {\r\n  random();\r\n}\r\n\r\n\r\n// Function pour rechercher des recettes de cuisine par nom.\r\nfunction byName(event) {\r\n  event.preventDefault();\r\n\r\n  let inputValue = input.value;\r\n\r\n  const urlByName = `${urlStart}${apiKey}${searchByName}${inputValue}`;\r\n\r\n  fetch(urlByName)\r\n    .then((response) => response.json())\r\n    .then(function (data) {\r\n      // console.log(data);\r\n\r\n      const { meals } = data;\r\n\r\n      list.innerHTML = \"\";\r\n      fenetre.innerHTML = \"\";\r\n\r\n      for (let meal of meals) {\r\n        console.log(meal);\r\n        let mealCard = new _components_MealCard_js__WEBPACK_IMPORTED_MODULE_0__.MealCard(meal);\r\n        list.appendChild(mealCard.content);\r\n        fenetre.appendChild(mealCard.recipe);\r\n      }\r\n    })\r\n    .catch(function (error) {\r\n      console.error(\r\n        \"Il y a eu un problème avec l'opération fetch Byname : \" + error.message\r\n      );\r\n    });\r\n}\r\n\r\n// Function pour rechercher les recettes par categorie\r\nfunction byCategory(event) {\r\n  event.preventDefault();\r\n\r\n  const category = document.querySelector(\"#category\").value;\r\n\r\n  const urlCategory = `${urlStart}${apiKey}${searchByCategory}${category}`;\r\n\r\n  fetch(urlCategory)\r\n    .then((response) => response.json())\r\n    .then(function (data) {\r\n      // console.log(data);\r\n\r\n      const { meals } = data;\r\n      list.innerHTML = \"\";\r\n      fenetre.innerHTML = \"\";\r\n\r\n      for (let meal of meals) {\r\n        byId(meal.idMeal);\r\n        console.log(meal);\r\n      }\r\n    })\r\n    .catch(function (error) {\r\n      console.error(\r\n        \"Il y a eu un problème avec l'opération fetch byCategory : \" +\r\n          error.message\r\n      );\r\n    });\r\n}\r\n\r\n// Function pour rechercher les recettes par pays\r\nfunction byArea(event) {\r\n  event.preventDefault();\r\n\r\n  const pays = document.querySelector(\"#pays\").value;\r\n\r\n  const urlArea = `${urlStart}${apiKey}${searchByArea}${pays}`;\r\n\r\n  fetch(urlArea)\r\n    .then((response) => response.json())\r\n    .then(function (data) {\r\n      const { meals } = data;\r\n      list.innerHTML = \"\";\r\n      fenetre.innerHTML = \"\";\r\n\r\n      for (let meal of meals) {\r\n        byId(meal.idMeal);\r\n      }\r\n    })\r\n    .catch(function (error) {\r\n      console.error(\r\n        \"Il y a eu un problème avec l'opération fetch byArea : \" + error.message\r\n      );\r\n    });\r\n}\r\n\r\n// Function pour rechercher des recettes de cuisine par ID.\r\nfunction byId(id) {\r\n  // Le début de l'URL pour utiliser MealDB\r\n  const urlStart = \"https://www.themealdb.com/api/json/v1/\";\r\n\r\n  // clef d'API\r\n  const apiKey = \"1\";\r\n  // Url pour une recherche par nom\r\n  const byId = \"/lookup.php?i=\";\r\n\r\n  // L'Url complete pour faire la recherche\r\n  const urlById = `${urlStart}${apiKey}${byId}${id}`;\r\n  // La requete à l'API\r\n  fetch(urlById, {\r\n    headers: {\r\n      Accept: \"application/json\",\r\n    },\r\n  })\r\n    .then((r) => r.json())\r\n    .then((data) => {\r\n      // console.log(data);\r\n      const { meals } = data;\r\n      // console.log(meals)\r\n      for (let meal of meals) {\r\n        // console.log(meal);\r\n        let mealCard = new _components_MealCard_js__WEBPACK_IMPORTED_MODULE_0__.MealCard(meal);\r\n        fenetre.appendChild(mealCard.recipe);\r\n        list.appendChild(mealCard.content);\r\n      }\r\n    })\r\n    .catch(function (error) {\r\n      console.error(\r\n        \"Il y a eu un problème avec l'opération fetch par ID : \" + error.message\r\n      );\r\n    });\r\n}\r\n\n\n//# sourceURL=webpack://application-de-recettes/./src/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;