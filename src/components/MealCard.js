export class MealCard {
  //  Le constructor
  constructor(meal) {
    this._idMeal = meal.idMeal;
    this._strMeal = meal.strMeal;
    this._strDrinkAlternate = meal.strDrinkAlternate;
    this._strCategory = meal.strCategory;
    this._strArea = meal.strArea;
    this._strMealThumb = meal.strMealThumb;
    this._strYoutube = meal.strYoutube;

    this._strInstructions = meal.strInstructions;
    this._strIngredients = [];
    this._strMeasures = [];

    for (let i = 1; i <= 20; i++) {
      const ingredient = meal["strIngredient" + i];
      const measure = meal["strMeasure" + i];

      if (ingredient !== "" && ingredient !== null) {
        this._strIngredients.push(ingredient);
        this._strMeasures.push(measure);
      }
    }
  }

  // Les méthodes

  // Function pour inserer en HTML la card
  get content() {
    const div = document.createElement("li");

    const markup = `
    <div class="item">
    <a id="recette" data-bs-toggle="modal" data-bs-target="#Id${this._idMeal}" class="card">
      <div class="thumb" style="background-image: url(${this._strMealThumb});"></div>
      <article>
      <h2>${this._strMeal}</h2>
      <p>Catégorie: ${this._strCategory}<br>
      Pays: ${this._strArea} </p>
      </article>
    </a>
    </div>
`;

    div.innerHTML = markup;
    return div;
  }
  get recipe() {
    const div = document.createElement("li");
    const ingredientList = this._strIngredients
      .map((ingredient, i) => {
        const measure = this._strMeasures[i];
        return `<li>${measure} ${ingredient}</li>`;
      })
      .join("");

    const markup = `
  <div class="modal fade" id="Id${this._idMeal}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
              <h1 class="modal-title fs-5" id="staticBackdropLabel">${this._strMeal}</h1>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
            </div>
            <div class="modal-body">
              <h2>Les ingredients</h2>
              <ul>
                  ${ingredientList}
              </ul>
              <hr>
              <h2>La recette</h2>
              <h5 id="recette">${this._strInstructions}</h5>
              <hr>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
`;
    div.innerHTML = markup;
    return div;
  }
}
