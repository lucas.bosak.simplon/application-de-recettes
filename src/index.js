import { MealCard } from "./components/MealCard.js";
// import { MealDetail } from "./components/MealDetail.js";
// clef d'API
const apiKey = "1";

// Le début de l'URL pour utiliser MealDB
const urlStart = "https://www.themealdb.com/api/json/v1/";

const form = document.querySelector("#Form");
const input = document.querySelector("#ByName");
const list = document.querySelector("#band");

const fenetre = document.querySelector("#fenetre");
const cat = document.querySelector("#form2");
const land = document.querySelector("#form3");

if (form !== null) form.addEventListener("submit", byName);
if (cat !== null) cat.addEventListener("submit", byCategory);
if (land !== null) land.addEventListener("submit", byArea);

// les url pour appeler l'API selon la manière de rechercher
const searchByName = "/search.php?s=";

const searchByCategory = "/filter.php?c=";

const searchByArea = "/filter.php?a=";

const searchRandom = "/random.php";

// Function pour afficher une recette au hasard.
function random() {
  const urlRandom = `${urlStart}${apiKey}${searchRandom}`;

  fetch(urlRandom)
    .then((response) => response.json())
    .then(function (data) {
      // console.log(data);

      const { meals } = data;

      for (let meal of meals) {
        let mealCard = new MealCard(meal);
        list.appendChild(mealCard.content);
        fenetre.appendChild(mealCard.recipe);
        // console.log(mealCard)
      }
    })
    .catch(function (error) {
      console.error(
        "Il y a eu un problème avec l'opération fetch Random : " + error.message
      );
    });
}
for (let i = 0; i < 6; i++) {
  random();
}


// Function pour rechercher des recettes de cuisine par nom.
function byName(event) {
  event.preventDefault();

  let inputValue = input.value;

  const urlByName = `${urlStart}${apiKey}${searchByName}${inputValue}`;

  fetch(urlByName)
    .then((response) => response.json())
    .then(function (data) {
      // console.log(data);

      const { meals } = data;

      list.innerHTML = "";
      fenetre.innerHTML = "";

      for (let meal of meals) {
        console.log(meal);
        let mealCard = new MealCard(meal);
        list.appendChild(mealCard.content);
        fenetre.appendChild(mealCard.recipe);
      }
    })
    .catch(function (error) {
      console.error(
        "Il y a eu un problème avec l'opération fetch Byname : " + error.message
      );
    });
}

// Function pour rechercher les recettes par categorie
function byCategory(event) {
  event.preventDefault();

  const category = document.querySelector("#category").value;

  const urlCategory = `${urlStart}${apiKey}${searchByCategory}${category}`;

  fetch(urlCategory)
    .then((response) => response.json())
    .then(function (data) {
      // console.log(data);

      const { meals } = data;
      list.innerHTML = "";
      fenetre.innerHTML = "";

      for (let meal of meals) {
        byId(meal.idMeal);
        console.log(meal);
      }
    })
    .catch(function (error) {
      console.error(
        "Il y a eu un problème avec l'opération fetch byCategory : " +
          error.message
      );
    });
}

// Function pour rechercher les recettes par pays
function byArea(event) {
  event.preventDefault();

  const pays = document.querySelector("#pays").value;

  const urlArea = `${urlStart}${apiKey}${searchByArea}${pays}`;

  fetch(urlArea)
    .then((response) => response.json())
    .then(function (data) {
      const { meals } = data;
      list.innerHTML = "";
      fenetre.innerHTML = "";

      for (let meal of meals) {
        byId(meal.idMeal);
      }
    })
    .catch(function (error) {
      console.error(
        "Il y a eu un problème avec l'opération fetch byArea : " + error.message
      );
    });
}

// Function pour rechercher des recettes de cuisine par ID.
function byId(id) {
  // Le début de l'URL pour utiliser MealDB
  const urlStart = "https://www.themealdb.com/api/json/v1/";

  // clef d'API
  const apiKey = "1";
  // Url pour une recherche par nom
  const byId = "/lookup.php?i=";

  // L'Url complete pour faire la recherche
  const urlById = `${urlStart}${apiKey}${byId}${id}`;
  // La requete à l'API
  fetch(urlById, {
    headers: {
      Accept: "application/json",
    },
  })
    .then((r) => r.json())
    .then((data) => {
      // console.log(data);
      const { meals } = data;
      // console.log(meals)
      for (let meal of meals) {
        // console.log(meal);
        let mealCard = new MealCard(meal);
        fenetre.appendChild(mealCard.recipe);
        list.appendChild(mealCard.content);
      }
    })
    .catch(function (error) {
      console.error(
        "Il y a eu un problème avec l'opération fetch par ID : " + error.message
      );
    });
}
